# RepoSense

# To run RepoSense, you will need to have the following installed:
    * JDK - ideally JDK 11 with JAVA_HOME set to the JDK 11 installation directory
    * Git - 2.14 or later
    * Node.js - 16.20.2

!IMPORTANT!
# Before starting RepoSense please run the following command in the command line:
        * "git config --global core.longpaths true"
    This will prevent report analysis errors in case of super long file names.

There are two necessary ways of parametrizing RepoSense in order to successfully run it:

# Using csv files located in config package:

    * `repo-config.csv` - contains information about repositories to be analyzed, please provide every reposiotry link in new line
    * `author-config.csv` - contains personalized information about users to have contributed in different git implementations (GitHub and GitLab eg.) this is mandatory to be able to merge authors between different git providers.

    In case of need of megering different users commiting on the same reposiotry please provide the following information in the `author-config.csv` file:

        * Author's Git Host ID - single username of one of the user that should be merged
        * Author's Emails - list of emails of all users that should be merged separated with ";" eg. "email1;email2;email3"
        * Author's Git Author Name - all of usernames of users that should be merged separated with ";" eg. "username1;username2;username3"

    Content of the `author-config.csv` file should look like this:
        line 1 : "Author's Git Host ID,Author's Emails,Author's Git Author Name"
        line 2 : "username1,email1;email2,username1;username2"

    Merging between different git providers is also possible but it will require another lines in author-config.csv with information regerding differnet git providers reposiotry.
    so the content of the `author-config.csv` file should look like this:
        line 1 : "Repository's Location,Author's Git Host ID,Author's Emails,Author's Git Author Name"
        line 2 : "https://github.com/mock/repo1.git,username1,email1;email2,username1;username2"
        line 3 : "https://github.com/mock/repo2.git,username1,email1;email2,username1;username2"

# Using CLI arguments in command line:
    The command to successfully run RepoSense using previously generated JAR is as follows:
        "java -jar RepoSense.jar --view -s 02/12/2023 -S -a C:\reposense -c ./config" with following parameters:

        * --view - this argument is used to open the report in the default browser right after the report is generated
        * -s - this argument is used to specify the date from which the report should be generated
        * -S - this argument is used to make the shallow clone of the repositories included in the report to improve the performance of the report generation
        * -a - this argument is used to specify where to place assets for report generation, by default it will be located in the jar file directory 
        * -c - this argument is used to specify the location of the config folder containing the csv files with the configuration of the repositories and authors.
    
    The command should be run from the location of the JAR file, we have included pregenerated version of it in the root directory of the project to make it easier to run RepoSense.

    However it is possible to run RepoSense using gradle, the only difference is that the command must necceserly be run from the project root directory and the command should be as follows:
        * "clean build run -x checkstyleMain -x test -Dargs="--view -s 15/12/2023 -S"" - this will start the analysis from 15/12/2023 and will open the report in the default browser.


